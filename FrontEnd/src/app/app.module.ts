import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { ShipmentService } from './services/shipment.service';
import { GetComponent } from './components/get/get.component';
import { PaymentComponent } from './components/payment/payment.component';
// external
import { NgxPaginationModule } from 'ngx-pagination';
// import { FilterPipeModule } from 'ngx-filter-pipe';
import { FilterPipe } from './pipes/filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    GetComponent,
    FilterPipe,
    PaymentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    // FilterPipeModule
  ],
  exports: [FormsModule, ReactiveFormsModule,],
  providers: [ShipmentService],
  bootstrap: [AppComponent]
})
export class AppModule {}
