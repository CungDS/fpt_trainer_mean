import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShipmentService {
  uri = 'http://localhost:9999';

  constructor(private http: HttpClient) { }

  getQuote(quote) {
    return this.http.post(`${this.uri}/getquote`, quote);
  }

  getShipment() {
    return this.http.get(`${this.uri}/getshipment`);
  }

  getShipmentById(id) {
    return this.http.get(`${this.uri}/getshipment/${id}`);
  }

  addShipment(shipment) {
    return this.http.post(`${this.uri}/createshipment`, shipment);
  }

  deleteShipment(id) {
    return this.http.delete(`${this.uri}/deleteshipment/${id}`);
  }
}
