import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(shipments: any, term: any): any {

    if(term === undefined) { return shipments; }

    return shipments.filter(item => {
      return item.data.origin.contact.name
        .toLowerCase()
        .includes(term.toLowerCase());
    });


    // const resultProduct = [];
    // for (const item of value) {
    //   if (item.indexOf(arg) > -1) {
    //     resultProduct.push(item);
    //   }
    // }
    // return resultProduct;
  }

}
