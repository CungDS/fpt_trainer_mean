import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Shipment } from '../../models/shipment.model';
import { ShipmentService } from '../../services/shipment.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


import { from } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  shipments: Shipment[];
  shipment: Shipment;
  searchForm: FormGroup;

  p = 1;
  // userFilter: '';
  constructor(
    private shipmentService: ShipmentService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.fetchShipment();
    this.searchForm = this.formBuilder.group({
      search: ['', [Validators.required]]
    });
  }

  get f() { return this.searchForm.controls; }

  fetchShipment() {
    this.shipmentService.getShipment().subscribe((data: Shipment[]) => {
      this.shipments = data;
    });
  }

  editShipment(id) {
    this.router.navigate([`/edit/${id}`]);
  }

  deleteShipment(id) {
    this.shipmentService.deleteShipment(id).subscribe(() => {
      this.fetchShipment();
    });
    alert(`Delete Success Shipment: ${id}`);
  }

  getShipmentById(id) {
    this.router.navigate([`/get/${id}`]);
  }

  onSearch() {
    const id = this.searchForm.get('search').value;
    if (this.searchForm.invalid) {
      return;
    }
    this.router.navigate([`/get/${id}`]);
  }
}
