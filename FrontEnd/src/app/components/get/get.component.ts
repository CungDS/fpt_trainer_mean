import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Shipment } from '../../models/shipment.model';
import { ShipmentService } from '../../services/shipment.service';

@Component({
  selector: 'app-get',
  templateUrl: './get.component.html',
  styleUrls: ['./get.component.css']
})
export class GetComponent implements OnInit {
  id: string;
  shipment: any = {};
  getForm: FormGroup;
  alertMessenger = false;

  constructor(private shipmentService: ShipmentService, private fb: FormBuilder, private route: ActivatedRoute) {
      this.createFrom();
    }

  createFrom() {
    this.getForm = this.fb.group({
      nameSender: [' ', [Validators.required]],
      emailSender: [' ', [Validators.required, Validators.email]],
      phoneSender: [' ', [Validators.required, Validators.minLength(10)]],
      countrySender: ['FR'],
      localitySender: [' ', [Validators.required]],
      postalcodeSender: [' ', [Validators.required]],
      addressSender: [' ', [Validators.required]],
      nameReceiver: [' ', [Validators.required]],
      emailReceiver: [' ', [Validators.required, Validators.email]],
      phoneReceiver: [' ', [Validators.required]],
      countryReceiver: ['FR'],
      localityReceiver: [' ', [Validators.required]],
      postalcodeReceiver: [' ', [Validators.required]],
      addressReceiver: [' ', [Validators.required]],
      height: [' ', [Validators.required]],
      width: [' ', [Validators.required]],
      length: [' ', [Validators.required]],
      weight: [' ', [Validators.required]],
      cost: [' ', [Validators.required]],
      ref: [' ', [Validators.required]]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.shipmentService.getShipmentById(this.id).subscribe(res => {
        // console.log(res[0].ref);
        console.log(res);
        if ( res['data'].ref !== '') {
          this.shipment = res;
          this.getForm.get('nameSender').setValue(this.shipment.data.origin.contact.name);
          this.getForm.get('emailSender').setValue(this.shipment.data.origin.contact.email);
          this.getForm.get('phoneSender').setValue(this.shipment.data.origin.contact.phone);
          this.getForm.get('countrySender').setValue(this.shipment.data.origin.address.country_code);
          this.getForm.get('localitySender').setValue(this.shipment.data.origin.address.locality);
          this.getForm.get('postalcodeSender').setValue(this.shipment.data.origin.address.postal_code);
          this.getForm.get('addressSender').setValue(this.shipment.data.origin.address.address_line1);
          this.getForm.get('nameReceiver').setValue(this.shipment.data.destination.contact.name);
          this.getForm.get('emailReceiver').setValue(this.shipment.data.destination.contact.email);
          this.getForm.get('phoneReceiver').setValue(this.shipment.data.destination.contact.phone);
          this.getForm.get('countryReceiver').setValue(this.shipment.data.destination.address.country_code);
          this.getForm.get('localityReceiver').setValue(this.shipment.data.destination.address.locality);
          this.getForm.get('postalcodeReceiver').setValue(this.shipment.data.destination.address.postal_code);
          this.getForm.get('addressReceiver').setValue(this.shipment.data.destination.address.address_line1);
          this.getForm.get('height').setValue(this.shipment.data.package.dimensions.height);
          this.getForm.get('width').setValue(this.shipment.data.package.dimensions.width);
          this.getForm.get('length').setValue(this.shipment.data.package.dimensions.length);
          this.getForm.get('weight').setValue(this.shipment.data.package.grossWeight.amount);
          this.getForm.get('ref').setValue(this.shipment.ref);
          this.getForm.get('cost').setValue(this.shipment.cost);
          } else {
            this.alertMessenger = true;
          }
      });
    });
  }

}
