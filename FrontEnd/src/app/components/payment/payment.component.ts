import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { async } from 'q';

declare let paypal;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  @ViewChild('paypal', {static: true }) paypalElement: ElementRef;

  constructor() { }

  product = {
    price: 77.77,
    name: 'Mua Quan Ao',
    description: 'hello',
    img: 'https://www.fpt-software.com/wp-content/uploads/sites/2/2018/03/logo_white.svg'
  };

  faidFor = false;

  ngOnInit() {
    paypal
      .Buttons({
      createOrder: (data, actions) => {
        return actions.order.create({
          purchase_units: [
            {
              description: this.product.description,
              amount: {
                currency_code: 'USD',
                value: this.product.price
              }
              // product: {
              //   price: this.product.price,
              //   description: this.product.description,
              //   total: 9999,
              //   quantity: 7
              // }
            }
          ]
        });
      },
      onApprove: async(data, actions) => {
        const order = await actions.order.capture();
        this.faidFor = true;
        console.log(order);
      },
      onError: err => {
        console.log(err);
      }
    }).render(this.paypalElement.nativeElement);
  }


}
