import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ShipmentService } from '../../services/shipment.service';

import { from } from 'rxjs';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  baseurl = 'http://localhost:9999';

  shipmentForm: FormGroup;
  submitted = false;
  messenger: string;
  isQuote = false;
  quoteId: string;
  isLength = false;
  isWidth = false;
  isHeight = false;
  isWeight = false;

  constructor(private shipmentService: ShipmentService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.shipmentForm = this.formBuilder.group({
      nameSender: ['', [Validators.required]],
      emailSender: ['', [Validators.required, Validators.email]],
      phoneSender: ['', [Validators.required, Validators.minLength(10)]],
      countrySender: ['FR'],
      localitySender: ['', [Validators.required]],
      postalcodeSender: ['', [Validators.required]],
      addressSender: ['', [Validators.required]],
      nameReceiver: ['', [Validators.required]],
      emailReceiver: ['', [Validators.required, Validators.email]],
      phoneReceiver: ['', [Validators.required, Validators.minLength(10)]],
      countryReceiver: ['FR'],
      localityReceiver: ['', [Validators.required]],
      postalcodeReceiver: ['', [Validators.required]],
      addressReceiver: ['', [Validators.required]],
      height: ['', [Validators.required, Validators.min(0.0000001)]],
      width: ['', [Validators.required, Validators.min(0.0000001)]],
      length: ['', [Validators.required, Validators.min(0.0000001)]],
      weight: ['', [Validators.required, Validators.min(0.0000001)]],
      cost: [' ']
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.shipmentForm.controls; }

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.shipmentForm.invalid) {
      return;
    }
    this.getQuote();
  }

  getQuote() {
    const quote = {
      data: {
        origin: {
          contact: {
            name: this.shipmentForm.get('nameSender').value,
            email: this.shipmentForm.get('emailSender').value,
            phone: this.shipmentForm.get('phoneSender').value
          },
          address: {
            country_code: this.shipmentForm.get('countrySender').value,
            locality: this.shipmentForm.get('localitySender').value,
            postal_code: this.shipmentForm.get('postalcodeSender').value,
            address_line1: this.shipmentForm.get('addressSender').value
          }
        },
        destination: {
          contact: {
            name: this.shipmentForm.get('nameReceiver').value,
            email: this.shipmentForm.get('emailReceiver').value,
            phone: this.shipmentForm.get('phoneReceiver').value
          },
          address: {
            country_code: this.shipmentForm.get('countryReceiver').value,
            locality: this.shipmentForm.get('localityReceiver').value,
            postal_code: this.shipmentForm.get('postalcodeReceiver').value,
            address_line1: this.shipmentForm.get('addressReceiver').value
          }
        },
        package: {
          dimensions: {
            height: this.shipmentForm.get('height').value,
            width: this.shipmentForm.get('width').value,
            length: this.shipmentForm.get('length').value,
            unit: 'cm'
          },
          grossWeight: {
            amount: this.shipmentForm.get('weight').value,
            unit: 'kg'
          }
        }
      }
    };
    this.shipmentService.getQuote(quote).subscribe(res => {
      this.messenger = res['data'][0].amount;
      this.quoteId = res['data'][0].id;
      this.shipmentForm.get('cost').setValue(this.messenger);
    });
    this.isQuote = true;
  }

  onCreate() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.shipmentForm.invalid) {
      return;
    }
    const shipment = {
      data: {
        quote: {
          id: this.quoteId
        },
        origin: {
          contact: {
            name: this.shipmentForm.get('nameSender').value,
            email: this.shipmentForm.get('emailSender').value,
            phone: this.shipmentForm.get('phoneSender').value
          },
          address: {
            country_code: this.shipmentForm.get('countrySender').value,
            locality: this.shipmentForm.get('localitySender').value,
            postal_code: this.shipmentForm.get('postalcodeSender').value,
            address_line1: this.shipmentForm.get('addressSender').value
          }
        },
        destination: {
          contact: {
            name: this.shipmentForm.get('nameReceiver').value,
            email: this.shipmentForm.get('emailReceiver').value,
            phone: this.shipmentForm.get('phoneReceiver').value
          },
          address: {
            country_code: this.shipmentForm.get('countryReceiver').value,
            locality: this.shipmentForm.get('localityReceiver').value,
            postal_code: this.shipmentForm.get('postalcodeReceiver').value,
            address_line1: this.shipmentForm.get('addressReceiver').value
          }
        },
        package: {
          dimensions: {
            height: this.shipmentForm.get('height').value,
            width: this.shipmentForm.get('width').value,
            length: this.shipmentForm.get('length').value,
            unit: 'cm'
          },
          grossWeight: {
            amount: this.shipmentForm.get('weight').value,
            unit: 'kg'
          }
        }
      }
    };
    this.addShipment(shipment);
  }

  addShipment(shipment) {
    this.shipmentService.addShipment(shipment).subscribe(() => {
      this.router.navigate(['/list']);
    });
  }

}
