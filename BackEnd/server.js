const mongoose = require('mongoose');
const mongoDB = 'mongodb://localhost:27017/MockProject';

const app = require('./src/app');

mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);

app.listen(9999);