const Controller = require('../controller.app');

describe("Controller App test", () => {
    it('has a module', () => {
        expect(Controller).toBeDefined();
    });

    describe('test randomRef', () => {
        it('randomRef length = 10', () => {
            const length = 10;
            const actual = Controller.randomRef();
            expect(actual).toHaveLength(length);
        });

        it('randomRef length != 10', () => {
            const length = 12;
            const actual = Controller.randomRef();
            expect(actual).not.toHaveLength(length);
        });
    });

    describe('test randomQuoteId', () => {
        it('randomQuoteId length = 13', () => {
            const length = 13;
            const actual = Controller.randomQuoteId();
            expect(actual).toHaveLength(length);
        });

        it('randomQuoteId length != 13', () => {
            const length = 10;
            const actual = Controller.randomQuoteId();
            expect(actual).not.toHaveLength(length);
        });
    });

    describe('test getShipmentCreateAt', () => {
        it('getShipmentCreateAt new Date', () => {
            const expected = new Date();
            const actual = Controller.getShipmentCreateAt();
            expect(expected).toEqual(actual);
        });
    });

    describe('test responDataGetQuote', () => {
        it('responDataGetQuote quoteid = 123, rate = 12', () => {
            const expected = { "data": [{ "id": 123, "amount": 12 }] };
            const actual = Controller.responDataGetQuote('123',12);
            expect(expected).toEqual(actual);
        });

        it('responDataGetQuote not quoteid = 123, rate = 12 ', () => {
            const expected = { "data": [{ "id": 123, "amount": 12 }] };
            const actual = Controller.responDataGetQuote('123', 123);
            expect(expected).not.toEqual(actual);
        });
    });

    describe('test responDataGetQuote', () => {
        it('responDataGetQuote ref = 123456, created_at = 2019, cost = 10', () => {
            const expected = { "data": [{ "ref": "123456", "created_at": 2019, "cost": 10 }] };
            const actual = Controller.responDataCreateShipment('123456', 2019, 10);
            expect(expected).toEqual(actual);
        });

        it('responDataGetQuote not ref = 123456, created_at = 2019, cost = 10', () => {
            const expected = { "data": [{ "ref": "123456", "created_at": 2019, "cost": 10 }] };
            const actual = Controller.responDataCreateShipment('123', 123);
            expect(expected).not.toEqual(actual);
        });
    });

});
