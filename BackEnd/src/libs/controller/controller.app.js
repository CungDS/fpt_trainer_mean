const crypto = require("crypto");
/*
*TODO  get RandomRef , Quote Id, CreateAt
*/

const randomRef = () => {
    return crypto.randomBytes(5).toString('hex');
}

const randomQuoteId = () => {
    let date = new Date();
    return date.valueOf();
}

const getShipmentCreateAt = () => {
    let current_date = new Date();
    // let formatted_date = current_date.getFullYear() + "-" + (current_date.getMonth() + 1) + "-" + current_date.getDate() + " " + current_date.getHours() + ":" + current_date.getMinutes() + ":" + current_date.getSeconds();
    return current_date;
}

// Response Data Structure
const responDataGetQuote = (quoteId, rates) => {
    return response = { "data": [{ "id": quoteId, "amount": rates }] };
}

const responDataCreateShipment = (ref, time, cost) => {
    return response = { "data": [{ "ref": ref, "created_at": time, "cost": cost }] };
}

module.exports = {
    randomRef,
    randomQuoteId,
    getShipmentCreateAt,
    responDataGetQuote,
    responDataCreateShipment
}