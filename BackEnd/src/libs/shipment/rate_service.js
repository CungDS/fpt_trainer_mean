const Rate = require('./rate_model');

const rates = [
    { weight: 250, price: '12.43', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 500, price: '12.43', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 750, price: '15.42', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 1000, price: '15.42', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 2000, price: '20.77', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 3000, price: '26.07', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 4000, price: '31.43', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 5000, price: '36.77', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 6000, price: '42.13', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 7000, price: '47.49', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 8000, price: '52.83', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 9000, price: '58.83', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 10000, price: '63.54', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 11000, price: '88.19', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 12000, price: '88.19', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 13000, price: '88.19', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 14000, price: '88.19', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 15000, price: '88.19', fromCountry: 'FR', toCountry: 'FR' },
    { weight: 15100, price: '100', fromCountry: 'FR', toCountry: 'FR' }
];

const kgTogram = 1000;

const getRates = async (data) => {
    let weightRequest = data.data.package.grossWeight.amount * kgTogram;
    let weight = await setWeight(weightRequest);
    let rates = await getRatesDocument(weight);
    let quote = rates[0].price;
    return quote;
};

const checkRateDocumentExist = () => {
    Rate.find({}, (err, docs) => {
        if(!docs.length){
            rateInsertDocument();
        }
    });
}

const rateInsertDocument =  () => {
    Rate.insertMany(rates, (err, res) => {
        if (err) throw new Error('Insert Failed');
    });
}

const setWeight =  (weight) => {
    return new Promise(resolve => {
        if (weight > 15000)
            weight = 15100;
        resolve(weight);
    });
}

const getRatesDocument = (weight) => {
    return new Promise((resolve, reject) => {
        Rate.find({ weight: { $gte: weight } }, { price: -1 }).exec((err, res) => {
            err ? reject(err) : resolve(res);
        });
    })
};

module.exports =  {
    rateInsertDocument,
    checkRateDocumentExist,
    setWeight,
    getRates,
    getRatesDocument
}
