const Shipment = require('./shipment_model');
const ShipmentService = require('./shipment_service');

module.exports = ShipmentService(Shipment);