// get ListShipment
const listShipment = Shipment => () => {
    return Shipment.find({});
}

// get Shipment By ID
const getShipmentByID = Shipment => (ref) => {
    return Shipment.findOne({ ref: ref});
}

// deleteShipment By ID
const deleteShipmentByID = Shipment => (ref) => {
    return Shipment.findOneAndRemove({ref: ref});
}

module.exports = Shipment => {
    return {
        listShipment: listShipment(Shipment),
        getShipmentByID: getShipmentByID(Shipment),
        deleteShipmentByID: deleteShipmentByID(Shipment)
    }
}






























