const mongoose = require('mongoose');
const mongoDB = "mongodb://127.0.0.1/MockProjectTest";
const Shipment = require('../shipment_model');

mongoose.connect(mongoDB);

describe("Shipment model test", () => {
    beforeAll(async () => {
        await Shipment.remove({});
    });

    afterEach(async () => {
        await Shipment.remove({});
    });

    afterAll(async () => {
        await mongoose.connection.close;
    });

    it('has a module', () => {
        expect(Shipment).toBeDefined();
    });

    describe("get Shipment", () => {
        it("get a shipment", async () => {
            const shipment = new Shipment({
                data: {
                    quote: {
                        id: 123456789
                    },
                    origin: {
                        contact: {
                            name: "La Redoute Contact",
                            email: "laredoute@example.com",
                            phone: "07 1234 5678",
                        },
                        address: {
                            country_code: "FR",
                            locality: "Anzin",
                            postal_code: "59410",
                            address_line1: "Rue Jean Jaures"
                        }
                    },
                    destination: {
                        contact: {
                            name: "Marquise de Pompadour",
                            email: "marquise-de-pompadour@example.com",
                            phone: "07 9876 5432"
                        },
                        address: {
                            country_code: "FR",
                            locality: "Marseille",
                            postal_code: "13006",
                            address_line1: "175 Rue de Rome"
                        }
                    },
                    package: {
                        dimensions: {
                            height: 10,
                            width: 10,
                            length: 10,
                            unit: "cm"
                        },
                        grossWeight: {
                            amount: 15,
                            unit: "kg"
                        }
                    }
                },
                ref: "4812bdeb8f",
                cost: 88.19
            });
        await shipment.save();

        const foundShipment = await Shipment.findOne({ ref: "4812bdeb8f" }, {cost: 1});
        const expected = 88.19;
        const actual = foundShipment.cost;
        expect(actual).toEqual(expected);
    });
});

describe("save Shipment", () => {
    it("save a shipment", async () => {
        const shipment = new Shipment(
            {
                data: {
                    quote: {
                        id: 123456789
                    },
                    origin: {
                        contact: {
                            name: "La Redoute Contact",
                            email: "laredoute@example.com",
                            phone: "07 1234 5678",
                        },
                        address: {
                            country_code: "FR",
                            locality: "Anzin",
                            postal_code: "59410",
                            address_line1: "Rue Jean Jaures"
                        }
                    },
                    destination: {
                        contact: {
                            name: "Marquise de Pompadour",
                            email: "marquise-de-pompadour@example.com",
                            phone: "07 9876 5432"
                        },
                        address: {
                            country_code: "FR",
                            locality: "Marseille",
                            postal_code: "13006",
                            address_line1: "175 Rue de Rome"
                        }
                    },
                    package: {
                        dimensions: {
                            height: 10,
                            width: 10,
                            length: 10,
                            unit: "cm"
                        },
                        grossWeight: {
                            amount: 15,
                            unit: "kg"
                        }
                    }
                },
                ref: "4812bdeb8f",
                cost: 88.19
            }
        );

        const saveShipment = await shipment.save();
        const expected = 88.19;
        const actual = saveShipment.cost;
        expect(actual).toEqual(expected);
    });
});

describe("Update Shipment", () => {
    it("Update a shipment", async () => {
        const shipment = new Shipment(
            {
                data: {
                    quote: {
                        id: 123456789
                    },
                    origin: {
                        contact: {
                            name: "La Redoute Contact",
                            email: "laredoute@example.com",
                            phone: "07 1234 5678",
                        },
                        address: {
                            country_code: "FR",
                            locality: "Anzin",
                            postal_code: "59410",
                            address_line1: "Rue Jean Jaures"
                        }
                    },
                    destination: {
                        contact: {
                            name: "Marquise de Pompadour",
                            email: "marquise-de-pompadour@example.com",
                            phone: "07 9876 5432"
                        },
                        address: {
                            country_code: "FR",
                            locality: "Marseille",
                            postal_code: "13006",
                            address_line1: "175 Rue de Rome"
                        }
                    },
                    package: {
                        dimensions: {
                            height: 10,
                            width: 10,
                            length: 10,
                            unit: "cm"
                        },
                        grossWeight: {
                            amount: 15,
                            unit: "kg"
                        }
                    }
                },
                ref: "4812bdeb8f",
                cost: 88.19
            }
        );
        await shipment.save();

        shipment.cost = 99;
        const updateShipment = await shipment.save();

        const expected = 99;
        const actual = updateShipment.cost;
        expect(actual).toEqual(expected);
    });
});
});
