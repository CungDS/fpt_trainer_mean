const mongoose = require('mongoose');
const mongoDB = "mongodb://127.0.0.1/MockProjectTest";
const Rate = require('../rate_model');

mongoose.connect(mongoDB);

describe("Rate model test", () => {
    beforeAll(async () => {
        await Rate.remove({});
    });

    afterEach(async () => {
        await Rate.remove({});
    });

    afterAll(async () => {
        await mongoose.connection.close;
    });

    it('has a module', () => {
        expect(Rate).toBeDefined();
    });

    describe("get Rate", () => {
        it("get a rate", async () => {
            const rate = new Rate({ weight: 10, price: 10, fromCountry: 'FR', toCountry: 'FR' });
            await rate.save();

            const foundRate = await Rate.findOne({ weight: 10 });
            const expected = 10;
            const actual = foundRate.weight;
            expect(actual).toEqual(expected);
        });
    });

    describe("save Rate", () => {
        it("save a rate", async () => {
            const rate = new Rate({ weight: 10, price: 10, fromCountry: 'FR', toCountry: 'FR' });

            const saveRate = await rate.save();
            const expected = 10;
            const actual = saveRate.weight;
            expect(actual).toEqual(expected);
        });
    });

    describe("Update Rate", () => {
        it("Update a rate", async () => {
            const rate = new Rate({ weight: 10, price: 10, fromCountry: 'FR', toCountry: 'FR' });
            await rate.save();

            rate.weight = 20;
            const updateRate = await rate.save();

            const expected = 20;
            const actual = updateRate.weight;
            expect(actual).toEqual(expected);
        });
    });
});