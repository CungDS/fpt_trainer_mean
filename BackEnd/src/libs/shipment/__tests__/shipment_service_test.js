const ShipmentService = require('../shipment_service');
const sinon = require('sinon');

describe("RateService test", () => {    // requied success
    it('has a module', () => {
        expect(ShipmentService).toBeDefined();
    });

    describe("List Shipment test", () => {
        it("list shipment", async () => {
            const MockModel = {
                find: sinon.spy()
            }

            const shipmentService = ShipmentService(MockModel);
            shipmentService.listShipment();
            const expected = true;
            const actual = MockModel.find.calledOnce;
            expect(actual).toEqual(expected);
        })
    });

    // describe("Get Shipment test", () => {
    //     it("get shipment by id", async () => {
    //         const MockModel = {
    //             find: sinon.spy()
    //         }

    //         const shipmentService = ShipmentService(MockModel);
    //         shipmentService.listShipment();
    //         const expected = true;
    //         const actual = MockModel.find.calledOnce;
    //         expect(actual).toEqual(expected);
    //     })
    // });
});