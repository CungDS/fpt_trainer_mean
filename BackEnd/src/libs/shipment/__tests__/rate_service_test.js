const RateService = require('../rate_service');
const sinon = require('sinon');

describe("RateService test", () => {    // requied success
    it('has a module', () => {
        expect(RateService).toBeDefined();
    });

    describe('test set Weight', () => {
        it('set weight <= 15000', async () => {
            const expected = 1500;
            const actual = await RateService.setWeight(1500);
            expect(expected).toEqual(actual);
        })

        it('set weight > 15000', async () => {
            const expected = 15100;
            const actual = await RateService.setWeight(16000);
            expect(expected).toEqual(actual);
        })

    });



});