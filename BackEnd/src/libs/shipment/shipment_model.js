const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let Shipment = new Schema({
    data: {
        quote: {
            id: String
        },
        origin: {
            contact: {
                name: String,
                email: String,
                phone: String
            },
            address: {
                country_code: String,
                locality: String,
                postal_code: String,
                address_line1: String
            }
        },
        destination: {
            contact: {
                name: String,
                email: String,
                phone: String
            },
            address: {
                country_code: String,
                locality: String,
                postal_code: String,
                address_line1: String
            }
        },
        package: {
            dimensions: {
                height: { type: Number, default: 0 },
                width: { type: Number, default: 0 },
                length: { type: Number, default: 0 },
                unit: String
            },
            grossWeight: {
                amount: Number,
                unit: String
            }
        }
    },
    ref: { type: String },
    cost: {type: Number}
});

module.exports = mongoose.model('Shipment', Shipment);

// ref: { type: String },
// created_at: { type: Date, default: Date.now },
// cost: { type: Number }