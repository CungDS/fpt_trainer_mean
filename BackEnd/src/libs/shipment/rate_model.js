const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let Rate = new Schema({
    weight: { type: Number, required: true },
    price: { type: Number, required: true },
    fromCountry: { type: String, required: true },
    toCountry: { type: String, required: true }
});

module.exports = mongoose.model('Rate', Rate);