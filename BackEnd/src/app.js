const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const crypto = require("crypto");
const Shipment = require('./libs/shipment/shipment_model');
const ShipmentService = require('./libs/shipment');
const Rates = require('./libs/shipment/rate_service');
const ControllerApp = require('./libs/controller/controller.app');
const app = express();
app.use(bodyParser.json())
app.use(cors());

app.use((request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});

// Create Rates Document Exist
Rates.checkRateDocumentExist();

// get Quote
app.post("/getquote", async (req, res, next) => {
    try {
        let { body } = req;
        const rates = await Rates.getRates(body);
        let getQuoteId = ControllerApp.randomQuoteId();
        res.json(ControllerApp.responDataGetQuote(getQuoteId, rates));
    } catch (e) {
        next(e);
    }
});

// create shipment
app.post("/createshipment", async (req, res, next) => {
    try {
        let { body } = req;
        const cost = await Rates.getRates(body);
        let createAt = ControllerApp.getShipmentCreateAt();
        body['ref'] = ControllerApp.randomRef();
        body['cost'] = cost;
        let shipment = new Shipment(body);
        shipment.save().then(shipment => {
            res.json(ControllerApp.responDataCreateShipment(shipment.ref, createAt, cost));
        }).catch(err => {
            res.send('Failed to create new records');
        });
    } catch (e) {
        next(e);
    }
});

// get All Shipment
app.get("/getshipment", async (req, res, next) => {
    try {
        const shipments = await ShipmentService.listShipment();
        res.json(shipments);
    } catch (e) {
        next(e);
    }
});

// get Shipment By ID
app.get("/getshipment/:id", async (req, res, next) => {
    try {
        let ref = req.params.id;
        const shipments = await ShipmentService.getShipmentByID(ref);
        (!shipments) ? res.json({ "data": { "ref": "" } }) : res.json(shipments);
    } catch (e) {
        next(e);
    }
});

// delete Shipment
app.delete("/deleteshipment/:id", async (req, res, next) => {
    try {
        let ref = req.params.id;
        const shipments = await ShipmentService.deleteShipmentByID(ref);
        (!shipments) ? res.json({ "data": [{ "status": "NOK", "message": "Shipment not found" }] }) : res.json({ "data": [{ "status": "OK", "message": "Shipment has been deleted" }] });
    } catch (e) {
        next(e);
    }
});

app.use((req, res, next) => {
    res.status(500).json({ error: "not found" });
});

app.use((err, req, next) => {
    res.status(500).json({ error: err.message });
});



module.exports = app;