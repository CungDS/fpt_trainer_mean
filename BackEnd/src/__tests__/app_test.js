const app = require("../app");
const request = require('supertest');
const mongoose = require('mongoose');
const mongoDB = "mongodb://127.0.0.1/MockProjectTest";

mongoose.connect(mongoDB);

describe("App test", () => {
    it('has a module', () => {
        expect(app).toBeDefined();
    });

    let server;

    beforeAll(async () => {
        server = app.listen(9998); // check port
    });

    afterAll(async () => {
        mongoose.connection.close();
        server.close(done);
    });

    describe("Shipment routes test", () => {
        it("can list shipment", async () => {
            await request(server).get('/getshipment').expect(200);
        });

        it("can get shipment by id", async () => {
            await request(server).get('/getshipment/4812bdeb8f').expect(200);
        });

        it("can post getQuote", async () => {
            let databody = {
                "data": {
                    "origin": {
                        "contact": {
                            "name": "La Redoute Contact",
                            "email": "laredoute@example.com",
                            "phone": "07 1234 5678"
                        },
                        "address": {
                            "country_code": "FR",
                            "locality": "Anzin",
                            "postal_code": "59410",
                            "address_line1": "Rue Jean Jaures"
                        }
                    },
                    "destination": {
                        "contact": {
                            "name": "Marquise de Pompadour",
                            "email": "marquise-de-pompadour@example.com",
                            "phone": "07 9876 5432"
                        },
                        "address": {
                            "country_code": "FR",
                            "locality": "Marseille",
                            "postal_code": "13006",
                            "address_line1": "175 Rue de Rome"
                        }
                    },
                    "package": {
                        "dimensions": {
                            "height": 10,
                            "width": 10,
                            "length": 10,
                            "unit": "cm"
                        },
                        "grossWeight": {
                            "amount": 2,
                            "unit": "kg"
                        }
                    }
                }
            };
            await request(server).post('/getquote').send(databody).expect(200);
        });

        it("can post create shipment", async () => {
            let databody = {
                "data": {
                    "quote": {
                        "id": "123456789"
                    },
                    "origin": {
                        "contact": {
                            "name": "La Redoute Contact",
                            "email": "laredoute@example.com",
                            "phone": "07 1234 5678"
                        },
                        "address": {
                            "country_code": "FR",
                            "locality": "Anzin",
                            "postal_code": "59410",
                            "address_line1": "Rue Jean Jaures"
                        }
                    },
                    "destination": {
                        "contact": {
                            "name": "Marquise de Pompadour",
                            "email": "marquise-de-pompadour@example.com",
                            "phone": "07 9876 5432"
                        },
                        "address": {
                            "country_code": "FR",
                            "locality": "Marseille",
                            "postal_code": "13006",
                            "address_line1": "175 Rue de Rome"
                        }
                    },
                    "package": {
                        "dimensions": {
                            "height": 10,
                            "width": 10,
                            "length": 10,
                            "unit": "cm"
                        },
                        "grossWeight": {
                            "amount": 15,
                            "unit": "kg"
                        }
                    }
                }
            };
            let response = {
                "data": [
                    {
                        "ref": "cc7c334c44",
                        "created_at": "2019-7-31 23:46:20",
                        "cost": 60
                    }
                ]
            };
            request(server).post('/createshipment').send(databody).expect(response);
        });

    });

});


    // describe('404', () => {
    //     it('returns 404', async () => {
    //         await request(server).post('/failed').expect(404);
    //     });
    // });
    

      // it("can delete shipment", async () => {
        //     let response = {
        //         "data": [
        //             {
        //                 "status": "OK",
        //                 "message": "Shipment has been deleted"
        //             }
        //         ]
        //     };
        //     await request(server).get('/deleteshipment/0d66a0eb1e').expect(200);
        // });